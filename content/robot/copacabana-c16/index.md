---
title: "COPacabana C16"
date: 2020-04-27T18:16:58+02:00
long_title: "Cable OPerated Robot w/ 16 Cables"
description: "A cable robot with up to 16 cables used for reconfiguration and pick-and-place with endless rotation."
motionpattern: 3r3t
organization: ustutt-isw
---
Initially two separate robots, now a single robot with up to 16 synchronized axes and endless rotation of the platform.